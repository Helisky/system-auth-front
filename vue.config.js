const webpack = require('webpack');

module.exports = {
  configureWebpack: {
    resolve: {
      fallback: {
        process: require.resolve('process/browser'),
        os: require.resolve('os-browserify/browser'),
        fs: false,
        path: false,
      },
    },
    plugins: [
      new webpack.ProvidePlugin({
        process: 'process/browser',
        os: 'os-browserify/browser',
      }),
    ],
  },
};
