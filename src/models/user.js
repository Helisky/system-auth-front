export default class User {
    constructor(username, email, password, birthdate, new_password, reset_token) {
      this.username = username;
      this.email = email;
      this.password = password;
      this.birthdate = birthdate;
      this.new_password = new_password;
      this.reset_token = reset_token;
    }
  }