import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/views/Home.vue';
import Login from '@/views/Login.vue';
import Register from '@/views/Register.vue';
import ForgotPassword from '@/views/ForgotPassword.vue';
import ResetPassword from '@/views/ResetPassword.vue';

Vue.use(Router);

export const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/home',
      component: Home
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/register',
      component: Register
    },
    {
      path: '/forgot-password',
      component: ForgotPassword
    },
    {
      path: '/reset-password/:token',
      component: ResetPassword
    },
    {
      path: '/profile',
      name: 'profile',
      // lazy-loaded
      component: () => import('@/views/Profile.vue'),
      meta:{
        requiresToken:true
      }
    },
    {
      path: '/palindromos',
      name: 'palindromo',
      // lazy-loaded
      component: () => import('@/views/Palindromo.vue')
    },
  ]
});


router.beforeEach((to, from, next) => {
  // const publicPages = ['/login', '/register', '/home', '/forgot-password', '/reset-password/{token}',];
  // const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');
  const requirestoken = to.matched.some(route => route.meta.requiresToken);

  // trying to access a restricted page + not logged in
  // redirect to login page
  if (requirestoken && !loggedIn) {
    next('/login');
  } else {
    next();
  }
});

// Exporta el router para que pueda ser utilizado en otros archivos
export default router