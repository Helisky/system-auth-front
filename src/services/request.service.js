// request.service.js
import axios from 'axios';

import authHeader from './auth-header';
require('dotenv').config();
// access url from environment
const API_URL = process.env.API_URL;

class requests {
  post(uri, data) {
    return axios.post(API_URL + uri, data, { headers: authHeader() });
  }

  get(uri) {
    return axios.get(API_URL + uri, { headers: authHeader() });
  }

  patch(uri, data) {
    return axios.patch(API_URL + uri, data, { headers: authHeader() });
  }

  delete(uri) {
    return axios.delete(API_URL + uri, { headers: authHeader() });
  }
}

export default new requests();
