import axios from 'axios';

require('dotenv').config();
// access url from environment
const API_URL = process.env.API_URL;

class AuthService {
  login(user) {
    const formData = new FormData();
    formData.append('username', user.username);
    formData.append('password', user.password);

    return axios
      .post(API_URL + 'login', formData)
      .then(response => {
        if (response.data.access_token) {
          localStorage.setItem('user', JSON.stringify(response.data));
        }
        return response.data;
      });
  }

  logout() {
    localStorage.removeItem('user');
  }

  forgot(user) {
    return axios.post(API_URL + 'reset-password',{
      email: user.email,
      birthdate: user.birthdate
    });
  }
  reset(user) {
    return axios.post(API_URL + 'reset_password/'+user.reset_token,{
      new_password: user.new_password,
      // reset_token: user.reset_token
    });
  }

  register(user) {
    return axios.post(API_URL + 'user', {
      username: user.username,
      email: user.email,
      password: user.password,
      birthdate: user.birthdate
    });
  }
}

export default new AuthService();