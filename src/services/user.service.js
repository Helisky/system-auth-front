import axios from 'axios';
import authHeader from './auth-header';

require('dotenv').config();
// access url from environment
const API_URL = process.env.API_URL;




class UserService {
  getPublicContent() {
    return axios.get(API_URL + 'all');
  }

  getUserBoard() {
    return axios.get(API_URL + 'user', { headers: authHeader() });
  }

  getModeratorBoard() {
    return axios.get(API_URL + 'mod', { headers: authHeader() });
  }

 post(uri, data){
  return axios.post(API_URL + uri, data).then(res => {
    if(res.status >= 200 ){
      return res.data;
    }
  })
 }
}

export default new UserService();